%global with_debug 1
%global with_bundled 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif

%if 0%{?rhel} > 7 && ! 0%{?fedora}
%define gobuild(o:) \
go build -buildmode pie -compiler gc -tags="rpm_crashtraceback no_openssl ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -compressdwarf=false -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \\n') -extldflags '%__global_ldflags'" -a -v -x %{?**};
%endif

%global provider        github
%global provider_tld    com
%global project         containers
%global repo            buildah
# https://github.com/projectatomic/buildah
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global commit          e94b4f98048e7371685731b97eefd6265e2f1fb3
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

Name:           %{repo}
Version:        1.5
Release:        8.git%{shortcommit}%{?dist}
Summary:        A command line tool used for creating OCI Images
License:        ASL 2.0
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz
# tracker bug: https://bugzilla.redhat.com/show_bug.cgi?id=CVE-2020-10696
# backported:  https://github.com/containers/buildah/commit/c61925b8936e93a5e900f91b653a846f7ea3a9ed.patch
Patch0: buildah-CVE-2020-10696.patch
ExclusiveArch:  x86_64 %{arm} aarch64 ppc64le s390x
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires:  git
BuildRequires:  glib2-devel
BuildRequires:  ostree-devel
BuildRequires:  glibc-static
BuildRequires:  go-md2man
BuildRequires:  gpgme-devel
BuildRequires:  device-mapper-devel
BuildRequires:  libassuan-devel
BuildRequires:  libseccomp-devel
Requires:       runc >= 1.0.0-26
Requires:       containers-common
Requires:       container-selinux
Provides:       %{repo} = %{version}-%{release}

%description
The %{name} package provides a command line tool which can be used to
* create a working container from scratch
or
* create a working container from an image as a starting point
* mount/umount a working container's root file system for manipulation
* save container's root file system layer to create a new image
* delete a working container or an image

%prep
%autosetup -Sgit -n %{name}-%{commit}

%build
mkdir _build
pushd _build
mkdir -p src/%{provider}.%{provider_tld}/%{project}
ln -s $(dirs +1 -l) src/%{import_path}
popd

mv vendor src

export GOPATH=$(pwd)/_build:$(pwd):%{gopath}
export BUILDTAGS='seccomp exclude_graphdriver_btrfs'
%gobuild -o %{name} %{import_path}/cmd/%{name}
make docs

%install
export GOPATH=$(pwd)/_build:$(pwd):%{gopath}
make DESTDIR=%{buildroot} PREFIX=%{_prefix} install install.completions

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}

%changelog
* Thu Jun 25 2020 Jindrich Novy <jnovy@redhat.com> - 1.5-8.gite94b4f9
- bump release to preserve upgrade path
- Related: #1821193

* Fri Apr 03 2020 Jindrich Novy <jnovy@redhat.com> - 1.5-4.gite94b4f9
- fix "CVE-2020-10696 buildah: crafted input tar file may lead to local file overwriting during image build process"
- Resolves: #1818127

* Tue Dec 18 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.5-3.gite94b4f9
- re-enable debuginfo

* Mon Dec 17 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.5-2.gite94b4f9
- go toolset not in scl anymore

* Fri Nov 23 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.5-1.gite94b4f9
- rebase

* Mon Nov 19 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.4-3.git608fa84
- fedora-like go compiler macro in buildrequires is enough

* Wed Oct 10 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 1.4-2.git608fa84
- rebase

* Mon Aug 13 2018 Lokesh Mandvekar <lsm5@redhat.com> - 1.3-3.git4888163
- Resolves: #1615611 - rebuild with gobuild tag 'no_openssl'

* Wed Aug 08 2018 Lokesh Mandvekar <lsm5@redhat.com> - 1.3-2.git4888163
- Resolves: #1614009 - built with updated scl-ized go-toolset dep
- build with %%gobuild

* Sun Aug 5 2018 Dan Walsh <dwalsh@redhat.com> - 1.3-1
- Bump to v1.3
- Vendor in lates containers/image
- build-using-dockerfile: let -t include transports again
- Block use of /proc/acpi and /proc/keys from inside containers
- Fix handling of --registries-conf
- Fix becoming a maintainer link
- add optional CI test fo darwin
- Don't pass a nil error to errors.Wrapf()
- image filter test: use kubernetes/pause as a "since"
- Add --cidfile option to from
- vendor: update containers/storage
- Contributors need to find the CONTRIBUTOR.md file easier
- Add a --loglevel option to build-with-dockerfile
- Create Development plan
- cmd: Code improvement
- allow buildah cross compile for a darwin target
- Add unused function param lint check
- docs: Follow man-pages(7) suggestions for SYNOPSIS
- Start using github.com/seccomp/containers-golang
- umount: add all option to umount all mounted containers
- runConfigureNetwork(): remove an unused parameter
- Update github.com/opencontainers/selinux
- Fix buildah bud --layers
- Force ownership of /etc/hosts and /etc/resolv.conf to 0:0
- main: if unprivileged, reexec in a user namespace
- Vendor in latest imagebuilder
- Reduce the complexity of the buildah.Run function
- mount: output it before replacing lastError
- Vendor in latest selinux-go code
- Implement basic recognition of the "--isolation" option
- Run(): try to resolve non-absolute paths using $PATH
- Run(): don't include any default environment variables
- build without seccomp
- vendor in latest runtime-tools
- bind/mount_unsupported.go: remove import errors
- Update github.com/opencontainers/runc
- Add Capabilities lists to BuilderInfo
- Tweaks for commit tests
- commit: recognize committing to second storage locations
- Fix ARGS parsing for run commands
- Add info on registries.conf to from manpage
- Switch from using docker to podman for testing in .papr
- buildah: set the HTTP User-Agent
- ONBUILD tutorial
- Add information about the configuration files to the install docs
- Makefile: add uninstall
- Add tilde info for push to troubleshooting
- mount: support multiple inputs
- Use the right formatting when adding entries to /etc/hosts
- Vendor in latest go-selinux bindings
- Allow --userns-uid-map/--userns-gid-map to be global options
- bind: factor out UnmountMountpoints
- Run(): simplify runCopyStdio()
- Run(): handle POLLNVAL results
- Run(): tweak terminal mode handling
- Run(): rename 'copyStdio' to 'copyPipes'
- Run(): don't set a Pdeathsig for the runtime
- Run(): add options for adding and removing capabilities
- Run(): don't use a callback when a slice will do
- setupSeccomp(): refactor
- Change RunOptions.Stdin/Stdout/Stderr to just be Reader/Writers
- Escape use of '_' in .md docs
- Break out getProcIDMappings()
- Break out SetupIntermediateMountNamespace()
- Add Multi From Demo
- Use the c/image conversion code instead of converting configs manually
- Don't throw away the manifest MIME type and guess again
- Consolidate loading manifest and config in initConfig
- Pass a types.Image to Builder.initConfig
- Require an image ID in importBuilderDataFromImage
- Use c/image/manifest.GuessMIMEType instead of a custom heuristic
- Do not ignore any parsing errors in initConfig
- Explicitly handle "from scratch" images in Builder.initConfig
- Fix parsing of OCI images
- Simplify dead but dangerous-looking error handling
- Don't ignore v2s1 history if docker_version is not set
- Add --rm and --force-rm to buildah bud
- Add --all,-a flag to buildah images
- Separate stdio buffering from writing
- Remove tty check from images --format
- Add environment variable BUILDAH_RUNTIME
- Add --layers and --no-cache to buildah bud
- Touch up images man
- version.md: fix DESCRIPTION
- tests: add containers test
- tests: add images test
- images: fix usage
- fix make clean error
- Change 'registries' to 'container registries' in man
- add commit test
- Add(): learn to record hashes of what we add
- Minor update to buildah config documentation for entrypoint
- Bump to v1.2-dev
- Add registries.conf link to a few man pages

* Tue Jul 24 2018 Lokesh Mandvekar <lsm5@redhat.com> - 1.2-3
- do not depend on btrfs-progs for rhel8

* Thu Jul 19 2018 Dan Walsh <dwalsh@redhat.com> - 1.2-2
- buildah does not require ostree

* Sun Jul 15 2018 Dan Walsh <dwalsh@redhat.com> 1.2-1
- Vendor in latest containers/image
- build-using-dockerfile: let -t include transports again
- Block use of /proc/acpi and /proc/keys from inside containers
- Fix handling of --registries-conf
- Fix becoming a maintainer link
- add optional CI test fo darwin
- Don't pass a nil error to errors.Wrapf()
- image filter test: use kubernetes/pause as a "since"
- Add --cidfile option to from
- vendor: update containers/storage
- Contributors need to find the CONTRIBUTOR.md file easier
- Add a --loglevel option to build-with-dockerfile
- Create Development plan
- cmd: Code improvement
- allow buildah cross compile for a darwin target
- Add unused function param lint check
- docs: Follow man-pages(7) suggestions for SYNOPSIS
- Start using github.com/seccomp/containers-golang
- umount: add all option to umount all mounted containers
- runConfigureNetwork(): remove an unused parameter
- Update github.com/opencontainers/selinux
- Fix buildah bud --layers
- Force ownership of /etc/hosts and /etc/resolv.conf to 0:0
- main: if unprivileged, reexec in a user namespace
- Vendor in latest imagebuilder
- Reduce the complexity of the buildah.Run function
- mount: output it before replacing lastError
- Vendor in latest selinux-go code
- Implement basic recognition of the "--isolation" option
- Run(): try to resolve non-absolute paths using $PATH
- Run(): don't include any default environment variables
- build without seccomp
- vendor in latest runtime-tools
- bind/mount_unsupported.go: remove import errors
- Update github.com/opencontainers/runc
- Add Capabilities lists to BuilderInfo
- Tweaks for commit tests
- commit: recognize committing to second storage locations
- Fix ARGS parsing for run commands
- Add info on registries.conf to from manpage
- Switch from using docker to podman for testing in .papr
- buildah: set the HTTP User-Agent
- ONBUILD tutorial
- Add information about the configuration files to the install docs
- Makefile: add uninstall
- Add tilde info for push to troubleshooting
- mount: support multiple inputs
- Use the right formatting when adding entries to /etc/hosts
- Vendor in latest go-selinux bindings
- Allow --userns-uid-map/--userns-gid-map to be global options
- bind: factor out UnmountMountpoints
- Run(): simplify runCopyStdio()
- Run(): handle POLLNVAL results
- Run(): tweak terminal mode handling
- Run(): rename 'copyStdio' to 'copyPipes'
- Run(): don't set a Pdeathsig for the runtime
- Run(): add options for adding and removing capabilities
- Run(): don't use a callback when a slice will do
- setupSeccomp(): refactor
- Change RunOptions.Stdin/Stdout/Stderr to just be Reader/Writers
- Escape use of '_' in .md docs
- Break out getProcIDMappings()
- Break out SetupIntermediateMountNamespace()
- Add Multi From Demo
- Use the c/image conversion code instead of converting configs manually
- Don't throw away the manifest MIME type and guess again
- Consolidate loading manifest and config in initConfig
- Pass a types.Image to Builder.initConfig
- Require an image ID in importBuilderDataFromImage
- Use c/image/manifest.GuessMIMEType instead of a custom heuristic
- Do not ignore any parsing errors in initConfig
- Explicitly handle "from scratch" images in Builder.initConfig
- Fix parsing of OCI images
- Simplify dead but dangerous-looking error handling
- Don't ignore v2s1 history if docker_version is not set
- Add --rm and --force-rm to buildah bud
- Add --all,-a flag to buildah images
- Separate stdio buffering from writing
- Remove tty check from images --format
- Add environment variable BUILDAH_RUNTIME
- Add --layers and --no-cache to buildah bud
- Touch up images man
- version.md: fix DESCRIPTION
- tests: add containers test
- tests: add images test
- images: fix usage
- fix make clean error
- Change 'registries' to 'container registries' in man
- add commit test
- Add(): learn to record hashes of what we add
- Minor update to buildah config documentation for entrypoint
- Add registries.conf link to a few man pages

* Sun Jun 10 2018 Dan Walsh <dwalsh@redhat.com> 1.1-1
- Drop capabilities if running container processes as non root
- Print Warning message if cmd will not be used based on entrypoint
- Update 01-intro.md
- Shouldn't add insecure registries to list of search registries
- Report errors on bad transports specification when pushing images
- Move parsing code out of common for namespaces and into pkg/parse.go
- Add disable-content-trust noop flag to bud
- Change freenode chan to buildah
- runCopyStdio(): don't close stdin unless we saw POLLHUP
- Add registry errors for pull
- runCollectOutput(): just read until the pipes are closed on us
- Run(): provide redirection for stdio
- rmi, rm: add test
- add mount test
- Add parameter judgment for commands that do not require parameters
- Add context dir to bud command in baseline test
- run.bats: check that we can run with symlinks in the bundle path
- Give better messages to users when image can not be found
- use absolute path for bundlePath
- Add environment variable to buildah --format
- rm: add validation to args and all option
- Accept json array input for config entrypoint
- Run(): process RunOptions.Mounts, and its flags
- Run(): only collect error output from stdio pipes if we created some
- Add OnBuild support for Dockerfiles
- Quick fix on demo readme
- run: fix validate flags
- buildah bud should require a context directory or URL
- Touchup tutorial for run changes
- Validate common bud and from flags
- images: Error if the specified imagename does not exist
- inspect: Increase err judgments to avoid panic
- add test to inspect
- buildah bud picks up ENV from base image
- Extend the amount of time travis_wait should wait
- Add a make target for Installing CNI plugins
- Add tests for namespace control flags
- copy.bats: check ownerships in the container
- Fix SELinux test errors when SELinux is enabled
- Add example CNI configurations
- Run: set supplemental group IDs
- Run: use a temporary mount namespace
- Use CNI to configure container networks
- add/secrets/commit: Use mappings when setting permissions on added content
- Add CLI options for specifying namespace and cgroup setup
- Always set mappings when using user namespaces
- Run(): break out creation of stdio pipe descriptors
- Read UID/GID mapping information from containers and images
- Additional bud CI tests
- Run integration tests under travis_wait in Travis
- build-using-dockerfile: add --annotation
- Implement --squash for build-using-dockerfile and commit
- Vendor in latest container/storage for devicemapper support
- add test to inspect
- Vendor github.com/onsi/ginkgo and github.com/onsi/gomega
- Test with Go 1.10, too
- Add console syntax highlighting to troubleshooting page
- bud.bats: print "$output" before checking its contents
- Manage "Run" containers more closely
- Break Builder.Run()'s "run runc" bits out
- util.ResolveName(): handle completion for tagged/digested image names
- Handle /etc/hosts and /etc/resolv.conf properly in container
- Documentation fixes
- Make it easier to parse our temporary directory as an image name
- Makefile: list new pkg/ subdirectoris as dependencies for buildah
- containerImageSource: return more-correct errors
- API cleanup: PullPolicy and TerminalPolicy should be types
- Make "run --terminal" and "run -t" aliases for "run --tty"
- Vendor github.com/containernetworking/cni v0.6.0
- Update github.com/containers/storage
- Update github.com/projectatomic/libpod
- Add support for buildah bud --label
- buildah push/from can push and pull images with no reference
- Vendor in latest containers/image
- Update gometalinter to fix install.tools error
- Update troubleshooting with new run workaround
- Added a bud demo and tidied up
- Attempt to download file from url, if fails assume Dockerfile
- Add buildah bud CI tests for ENV variables
- Re-enable rpm .spec version check and new commit test
- Update buildah scratch demo to support el7
- Added Docker compatibility demo
- Update to F28 and new run format in baseline test
- Touchup man page short options across man pages
- Added demo dir and a demo. chged distrorlease
- builder-inspect: fix format option
- Add cpu-shares short flag (-c) and cpu-shares CI tests
- Minor fixes to formatting in rpm spec changelog
- Fix rpm .spec changelog formatting
- CI tests and minor fix for cache related noop flags
- buildah-from: add effective value to mount propagation

* Mon May 7 2018 Dan Walsh <dwalsh@redhat.com> 1.0-1
- Remove buildah run cmd and entrypoint execution
- Add Files section with registries.conf to pertinent man pages
- Force "localhost" as a default registry
- Add --compress, --rm, --squash flags as a noop for bud
- Add FIPS mode secret to buildah run and bud
- Add config --comment/--domainname/--history-comment/--hostname
- Add support for --iidfile to bud and commit
- Add /bin/sh -c to entrypoint in config
- buildah images and podman images are listing different sizes
- Remove tarball as an option from buildah push --help
- Update entrypoint behaviour to match docker
- Display imageId after commit
- config: add support for StopSignal
- Allow referencing stages as index and names
- Add multi-stage builds support
- Vendor in latest imagebuilder, to get mixed case AS support
- Allow umount to have multi-containers
- Update buildah push doc
- buildah bud walks symlinks
- Imagename is required for commit atm, update manpage

* Thu May 03 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.16-3.git532e267
- Resolves: #1573681
- built commit 532e267

* Tue Apr 10 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.16.0-2.git6f7d05b
- built commit 6f7d05b

* Wed Apr 4 2018 Dan Walsh <dwalsh@redhat.com> 0.16-1
-   Add support for shell
-   Vendor in latest containers/image
-    	 docker-archive generates docker legacy compatible images
-	 Do not create $DiffID subdirectories for layers with no configs
- 	 Ensure the layer IDs in legacy docker/tarfile metadata are unique
-	 docker-archive: repeated layers are symlinked in the tar file
-	 sysregistries: remove all trailing slashes
-	 Improve docker/* error messages
-	 Fix failure to make auth directory
-	 Create a new slice in Schema1.UpdateLayerInfos
-	 Drop unused storageImageDestination.{image,systemContext}
-	 Load a *storage.Image only once in storageImageSource
-	 Support gzip for docker-archive files
-	 Remove .tar extension from blob and config file names
-	 ostree, src: support copy of compressed layers
-	 ostree: re-pull layer if it misses uncompressed_digest|uncompressed_size
-	 image: fix docker schema v1 -> OCI conversion
-	 Add /etc/containers/certs.d as default certs directory
-  Change image time to locale, add troubleshooting.md, add logo to other mds
-   Allow --cmd parameter to have commands as values
-   Document the mounts.conf file
-   Fix man pages to format correctly
-   buildah from now supports pulling images using the following transports:
-   docker-archive, oci-archive, and dir.
-   If the user overrides the storage driver, the options should be dropped
-   Show Config/Manifest as JSON string in inspect when format is not set
-   Adds feature to pull compressed docker-archive files

* Tue Feb 27 2018 Dan Walsh <dwalsh@redhat.com> 0.15-1
- Fix handling of buildah run command options

* Mon Feb 26 2018 Dan Walsh <dwalsh@redhat.com> 0.14-1
- If commonOpts do not exist, we should return rather then segfault
- Display full error string instead of just status
- Implement --volume and --shm-size for bud and from
- Fix secrets patch for buildah bud
- Fixes the naming issue of blobs and config for the dir transport by removing the .tar extension

* Mon Feb 26 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.13-1.git99066e0
- use correct version

* Mon Feb 26 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.12-4.git99066e0
- enable debuginfo

* Mon Feb 26 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.12-3.git99066e0
- BR: libseccomp-devel

* Mon Feb 26 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.12-2.git99066e0
- Resolves: #1548535
- built commit 99066e0

* Mon Feb 12 2018 Dan Walsh <dwalsh@redhat.com> 0.12-1
- Added handing for simpler error message for Unknown Dockerfile instructions.
- Change default certs directory to /etc/containers/certs.dir
- Vendor in latest containers/image
- Vendor in latest containers/storage
- build-using-dockerfile: set the 'author' field for MAINTAINER
- Return exit code 1 when buildah-rmi fails
- Trim the image reference to just its name before calling getImageName
- Touch up rmi -f usage statement
- Add --format and --filter to buildah containers
- Add --prune,-p option to rmi command
- Add authfile param to commit
- Fix --runtime-flag for buildah run and bud
- format should override quiet for images
- Allow all auth params to work with bud
- Do not overwrite directory permissions on --chown
- Unescape HTML characters output into the terminal
- Fix: setting the container name to the image
- Prompt for un/pwd if not supplied with --creds
- Make bud be really quiet
- Return a better error message when failed to resolve an image
- Update auth tests and fix bud man page

* Mon Feb 05 2018 Lokesh Mandvekar <lsm5@redhat.com> - 0.11-3.git49095a8
- Resolves: #1542236 - add ostree and bump runc dep

* Thu Feb 01 2018 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.11-2.git49095a8
- rebased to 49095a83f8622cf69532352d183337635562e261

* Tue Jan 16 2018 Dan Walsh <dwalsh@redhat.com> 0.11-1
- Add --all to remove containers
- Add --all functionality to rmi
- Show ctrid when doing rm -all
- Ignore sequential duplicate layers when reading v2s1
- Lots of minor bug fixes
- Vendor in latest containers/image and containers/storage

* Sat Dec 23 2017 Dan Walsh <dwalsh@redhat.com> 0.10-2
- Fix checkin

* Sat Dec 23 2017 Dan Walsh <dwalsh@redhat.com> 0.10-1
- Display Config and Manifest as strings
- Bump containers/image
- Use configured registries to resolve image names
- Update to work with newer image library
- Add --chown option to add/copy commands

* Tue Dec 12 2017 Lokesh Mandvekar <lsm5@redhat.com> - 0.9-2.git04ea079
- build for all arches

* Sat Dec 2 2017 Dan Walsh <dwalsh@redhat.com> 0.9-1
- Allow push to use the image id
- Make sure builtin volumes have the correct label

* Wed Nov 22 2017 Dan Walsh <dwalsh@redhat.com> 0.8-1
- Buildah bud was failing on SELinux machines, this fixes this
- Block access to certain kernel file systems inside of the container

* Thu Nov 16 2017 Dan Walsh <dwalsh@redhat.com> 0.7-1
- Ignore errors when trying to read containers buildah.json for loading SELinux reservations
-     Use credentials from kpod login for buildah
- Adds support for converting manifest types when using the dir transport
- Rework how we do UID resolution in images
- Bump github.com/vbatts/tar-split
- Set option.terminal appropriately in run

* Thu Nov 16 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5-5.gitf7dc659
- revert building for s390x, it is intended for rhel 7.5

* Wed Nov 15 2017 Dan Walsh <dwalsh@redhat.com> 0.5-4
- Add requires for container-selinux

* Mon Nov 13 2017 Frantisek Kluknavsky <fkluknav@redhat.com> - 0.5-3.gitf7dc659
- build for s390x, https://bugzilla.redhat.com/show_bug.cgi?id=1482234

* Wed Nov 08 2017 Dan Walsh <dwalsh@redhat.com> 0.5-2
-  Bump github.com/vbatts/tar-split
-  Fixes CVE That could allow a container image to cause a DOS

* Tue Nov 07 2017 Dan Walsh <dwalsh@redhat.com> 0.5-1
-  Add secrets patch to buildah
-  Add proper SELinux labeling to buildah run
-  Add tls-verify to bud command
-  Make filtering by date use the image's date
-  images: don't list unnamed images twice
-  Fix timeout issue
-  Add further tty verbiage to buildah run
-  Make inspect try an image on failure if type not specified
-  Add support for `buildah run --hostname`
-  Tons of bug fixes and code cleanup

* Tue Nov  7 2017 Nalin Dahyabhai <nalin@redhat.com> - 0.4-2.git01db066
- bump to latest version
- set GIT_COMMIT at build-time

* Fri Sep 22 2017 Dan Walsh <dwalsh@redhat.com> 0.4-1.git9cbccf88c
-   Add default transport to push if not provided
-   Avoid trying to print a nil ImageReference
-   Add authentication to commit and push
-   Add information on buildah from man page on transports
-   Remove --transport flag
-   Run: do not complain about missing volume locations
-   Add credentials to buildah from
-   Remove export command
-   Run(): create the right working directory
-   Improve "from" behavior with unnamed references
-   Avoid parsing image metadata for dates and layers
-   Read the image's creation date from public API
-   Bump containers/storage and containers/image
-   Don't panic if an image's ID can't be parsed
-   Turn on --enable-gc when running gometalinter
-   rmi: handle truncated image IDs

* Fri Sep 22 2017 Lokesh Mandvekar <lsm5@redhat.com> - 0.4-1.git9cbccf8
- bump to v0.4

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3-4.gitb9b2a8a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.3-3.gitb9b2a8a
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jul 20 2017 Dan Walsh <dwalsh@redhat.com> 0.3-2.gitb9b2a8a7e
- Bump for inclusion of OCI 1.0 Runtime and Image Spec

* Tue Jul 18 2017 Dan Walsh <dwalsh@redhat.com> 0.2.0-1.gitac2aad6
-   buildah run: Add support for -- ending options parsing 
-   buildah Add/Copy support for glob syntax
-   buildah commit: Add flag to remove containers on commit
-   buildah push: Improve man page and help information
-   buildah run: add a way to disable PTY allocation
-   Buildah docs: clarify --runtime-flag of run command
-   Update to match newer storage and image-spec APIs
-   Update containers/storage and containers/image versions
-   buildah export: add support
-   buildah images: update commands
-   buildah images: Add JSON output option
-   buildah rmi: update commands
-   buildah containers: Add JSON output option
-   buildah version: add command
-   buildah run: Handle run without an explicit command correctly
-   Ensure volume points get created, and with perms
-   buildah containers: Add a -a/--all option

* Wed Jun 14 2017 Dan Walsh <dwalsh@redhat.com> 0.1.0-2.git597d2ab9
- Release Candidate 1
- All features have now been implemented.

* Fri Apr 14 2017 Dan Walsh <dwalsh@redhat.com> 0.0.1-1.git7a0a5333
- First package for Fedora
